var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations
var MenuCommentSchema = Schema({
        comment: {type: String, required: false, trim: true},
        userId:{type: String, required:true},
        picture:{type: String, required:false},
        menuItemId:{type: String, required:true}
    },
    {
        timestamps:true,
        collection: "MenuComment"});

//Deep Population Declarations
MenuCommentSchema.plugin(deepPopulate,{});

//Model Declarations
var MenuComment = mongoose.model('MenuComment', MenuCommentSchema);

module.exports=MenuComment;
