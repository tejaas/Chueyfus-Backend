
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
//Schema Declarations
var LoyaltySchema = Schema({
            name: {type: String, required: false, trim: true},
            description: {type: String, required: true, trim: true},
            image: {type: String, required: false, trim: true},
            deleted: {type: Boolean, required: false, default: false},
            stamps: {type: Number, required: false, default: 5}

    },
    {
            timestamps:true,
            collection: "Loyalty"});


//Model Declarations
var Loyalty = mongoose.model('Loyalty', LoyaltySchema);



module.exports=Loyalty;
