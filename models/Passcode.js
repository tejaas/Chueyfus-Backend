var connection = require('../mongooseConnection');
var mongoose = connection.mongoose;
var Schema = mongoose.Schema;
//Schema Declarations
var PasscodeSchema = Schema({
        passcode: {type: String, trim: true,unique:true},
        name: {type: String, trim: true},
        sunday: {type: Number, trim: true, default: 0},
        monday: {type: Number, trim: true, default: 0},
        tuesday: {type: Number, trim: true, default: 0},
        wednesday: {type: Number, trim: true, default: 0},
        thursday: {type: Number, trim: true, default: 0},
        friday: {type: Number, trim: true, default: 0},
        saturday: {type: Number, trim: true, default: 0},
    },
    {
        timestamps: true,
        collection: "Passcode"
    });


//Model Declarations
var Passcode = mongoose.model('Passcode', PasscodeSchema);

module.exports = Passcode;
