
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations
var TequilaSchema = Schema({
            name: {type: String, required: false, trim: true},
            image: {type: String, required: true, trim: true},
            description: {type: String, required: true, trim: true},
            imageThumb: {type: String,  required: false, trim: true},
            price: {type: String, required: true, trim:true},
            tourId : {type:String,required:true},
            deleted: {type: Boolean, required: true, default: false},
            order: {type: Number, required: false}
    },
    {
            timestamps:true,
            collection: "Tequila"});

TequilaSchema.statics.toggleChecked = function(id,cb){

    this.findByIdAndUpdate(id, {$set: {checked: !this.checked} }, function(err, book) {
        cb();
    })

};
//Deep Population Declarations
TequilaSchema.plugin(deepPopulate,{});

//Model Declarations
var Tequila = mongoose.model('Tequila', TequilaSchema);



module.exports=Tequila;
