
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;

//Schema Declarations
var UserSchema = Schema({
        name: {type: String, trim: true},
        username: {type: String, trim: true, lowercase:true},
        email: {type: String, required: true, trim: true, lowercase:true},
        password: {type: String,trim: true},
        dob: {type: Date, required:false },
        accountType: {type: String, trim: true},
        fbUserId: {type: String},
        checked:[{type: Schema.Types.ObjectId , required: false}],
        picture:{type: String, required:false },
        loyalty:[Schema({
                loyaltyId: {type: Schema.Types.ObjectId, required: false},
                value: {type: Number, default:0},
                updated:[{type: String , required: false}],
                used:{type: Boolean, default:false}
            },
            {
                    timestamps:true
            })]
    },
    {
            timestamps:true,
            collection: "User"});

//Deep Population Declarations
UserSchema.plugin(deepPopulate,{});

//Model Declarations
var User = mongoose.model('User', UserSchema);

module.exports=User;
