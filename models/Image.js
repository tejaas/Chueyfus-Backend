var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations
var ImageSchema = Schema({
        imageId: {type: String, required: false, trim: true},
        userId:{type: String, required: false, trim: true},
        deleted:{type: Boolean, required: false, trim: true, default:false}
    },
    {
        timestamps:true,
        collection: "Image"});

//Deep Population Declarations
ImageSchema.plugin(deepPopulate,{});

//Model Declarations
var Image = mongoose.model('Image', ImageSchema);

module.exports=Image;
