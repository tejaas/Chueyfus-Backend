
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations
var ItemSchema = Schema({
        name: {type: String, required: false, trim: true},
        image: {type: String, required: true, trim: true},
        description: {type: String, required: true, trim: true},
        imageThumb: {type: String,  required: false, trim: true},
        catId : {type:String,required:true},
        deleted: {type: Boolean, required: true, default: false},
        order: {type: Number, required: false}

    },
    {
            timestamps:true,
            collection: "Item"});

//Deep Population Declarations
ItemSchema.plugin(deepPopulate,{});

//Model Declarations
var Item = mongoose.model('Item', ItemSchema);

module.exports=Item;
