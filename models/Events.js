
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
var deepPopulate=connection.deepPopulate;
//Schema Declarations
var TourSchema = Schema({
            name: {type: String, required: false, trim: true},
            image: {type: String, required: true, trim: true},
            description: {type: String, required: true, trim: true},
            imageThumb: {type: String,  required: false, trim: true},
            deleted: {type: Boolean, required: true, default: false},
            order: {type: Number, required: false}
            

    },
    {
            timestamps:true,
            collection: "Tour"});

//Deep Population Declarations
TourSchema.plugin(deepPopulate,{});

//Model Declarations
var Tour = mongoose.model('Tour', TourSchema);

module.exports=Tour;
