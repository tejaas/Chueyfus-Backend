
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
//Schema Declarations
var RestaurantInfoSchema = Schema({
        name: {type: String, required: false, trim: true},
        location:Schema({
            lat: {type: Number, required: true, trim: true},
            long: {type: Number, required: true, trim: true}
        }),
        address: {type: String, required: true, trim: true},
        timing:[Schema({
            openingTime: {type: String, required: true, trim: true},
            closingTime: {type: String, required: true, trim: true}
        })]
    },
    {
        timestamps:true,
        collection: "RestaurantInfo"});


//Model Declarations
var RestaurantInfoSchema = mongoose.model('RestaurantInfo', RestaurantInfoSchema);

module.exports=RestaurantInfoSchema;

