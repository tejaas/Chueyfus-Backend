
var connection= require('../mongooseConnection');
var mongoose=connection.mongoose;
var Schema=mongoose.Schema;
//Schema Declarations
var DeviceTokenSchema = Schema({
        deviceToken: {type: String, trim: true}
    },
    {
            timestamps:true,
            collection: "DeviceToken"});


//Model Declarations
var DeviceToken = mongoose.model('DeviceToken', DeviceTokenSchema);

module.exports=DeviceToken;
