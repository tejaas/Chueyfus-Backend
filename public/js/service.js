angular.module('myApp')
    .factory('Category', function ($resource, catApi) {
        return $resource(catApi + '/category/:id', null, {
            'update': {method: 'PUT'},
            'reorder': {
                method: 'PUT',
                'url': catApi + '/reorderCategory',
                'isArray': false
            }
        }); // Note the full endpoint address
    })
    .factory('User', function ($resource, catApi) {
        return $resource(catApi + '/user')
    })
    .factory('Login', function ($resource, catApi) {
        return $resource(catApi + '/adminLogin')
    })
    .factory('logout', function ($resource, catApi) {
        return $resource(catApi + '/logout')
    })
    .factory('RestaurantInfo', function ($resource, catApi) {
        return $resource(catApi + '/restaurantInfo', null, {
            'update': {method: 'PUT'}
        }); // Note the full endpoint address
    })
    .factory('Comment', function ($resource, catApi) {
        return $resource(catApi + '/comment/:id', null, {
            'update': {method: 'PUT'}
        })
    })

    .factory('PushNotification', function ($resource, catApi) {
        return $resource(catApi + '/sendPush', null, {
            'update': {method: 'PUT'}
        }); // Note the full endpoint address
    })

    .factory('Image', function ($resource, catApi) {
        return $resource(catApi + '/image', null, {
            'update': {method: 'PUT'}
        })
    })

    .factory('MenuItem', function ($resource, catApi) {
        return $resource(catApi + '/item/:id', null, {
            'update': {method: 'PUT'},
            'reorder': {
                method: 'PUT',
                'url': catApi + '/reorderItems',
                'isArray': false
            }
        }); // Note the full endpoint address
    })
    .factory('TourItem', function ($resource, catApi) {
        return $resource(catApi + '/tour/:id', null, {
            'update': {method: 'PUT'},
            'reorder': {
                method: 'PUT',
                'url': catApi + '/reorderTour',
                'isArray': false
            }
        }); // Note the full endpoint address
    })
    .factory('Passcode', function ($resource, catApi) {
        return $resource(catApi + '/passcode/:itemToBeDeletedId', {
            itemToBeDeletedId: '@itemToBeDeletedId',
            itemToBeReset: '@itemToBeReset'
        }, {
            'update': {
                method: 'PUT',
                'url': catApi + '/passcode/updateInfo',
                'isArray': false
            },
            'resetToZero': {
                method: 'PUT',
                'url': catApi + '/passcode/reset/:itemToBeReset',
                'isArray': false
            },

        }); // Note the full endpoint address
    })
    .factory('Loyalty', function ($resource, catApi) {
        return $resource(catApi + '/loyalty', null, {
            'update': {method: 'PUT'}
        });
    })
    .factory('TourDetails', function ($resource, catApi) {
        return $resource(catApi + '/tequila/:id', null, {
            'update': {method: 'PUT'},
            'reorder': {
                method: 'PUT',
                'url': catApi + '/reorderTequila',
                'isArray': false
            }
        }); // Note the full endpoint address
    })
    .factory('EditContactInfo', function ($resource, catApi) {
        return $resource(catApi + '/tequila/:id', null, {
            'update': {method: 'PUT'}
        }); // Note the full endpoint address
    })

