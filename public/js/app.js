'use strict';
// Declare app level module which depends on filters, and services
var server = 'http://35.160.70.94/uploads/';
// var server = 'http://localhost:8080/uploads/';
var app = angular.module('myApp', [
    'ngRoute', 'angular.filter', 'ngResource', 'ngFileUpload', 'timer', 'ngSanitize'
])
app.constant('catApi', '');


app.config(function ($routeProvider, $locationProvider, $httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push(function ($q, $location) {
        return {
            'responseError': function (rejection) {
                if (rejection.status == 302) {
                    console.log(JSON.parse(rejection.data).redirect);
                    if (JSON.parse(rejection.data).redirect == 'login') {
                        $location.path('/login');
                    }
                    else if (JSON.parse(rejection.data).redirect == 'adminPanel') {
                        $location.path('/adminPanel');
                    }
                }
                return $q.reject(rejection);
            }
        };
    });

    $routeProvider
        .when('/adminPanel', {
            templateUrl: 'partials/adminPanel'
        })
        .when('/login', {
            templateUrl: 'partials/adminLogin'
        })
        .when('/privacy', {
            templateUrl: 'partials/privacy'
        })
        .otherwise({
            redirectTo: '/login'
        });
    $locationProvider.html5Mode(true);
})
    .run(function ($http, $rootScope, catApi,$location) {

        $rootScope.$on('$locationChangeStart', function(event,next, current) {

            if(typeof $rootScope.loggedIn=='undefined')
            {
                //do nothing
            }
            else
            {

                if($rootScope.loggedIn && $location.path()=='/login')
                {

                    event.defaultPrevented=true;
                }

                if(!$rootScope.loggedIn && $location.path()=='/adminPanel')
                {

                    event.defaultPrevented=true;
                }

            }
        });

        $rootScope.catApi = catApi;
    })
    .controller('categoryCtrl', categoryCtrl)
    .controller('addMenuItemCtrl', addMenuItemCtrl)
    .controller('addTourCtrl', addTourCtrl)
    .controller('addTourDetailsCtrl', addTourDetailsCtrl)
    .controller('contactInfoCtrl', contactInfoCtrl)
    .controller('HeaderCtrl', HeaderCtrl)
    .controller('FanCommentCtrl', FanCommentCtrl)
    .controller('PushNotificationsCtrl', PushNotificationsCtrl)
    .controller('ManagePhotosCtrl', ManagePhotosCtrl)
    .controller('userCtrl', userCtrl)
    .controller('LoyaltyCtrl', LoyaltyCtrl)
    .controller('PasscodeCtrl', PasscodeCtrl)
    .controller('AdminLoginCtrl', AdminLoginCtrl)

function userCtrl(User,$rootScope) {
    var userCtrl = this;
    $rootScope.LoggedIn=true;
    User.query({}, function (data) {
        userCtrl.allUsers = data;
        console.log(userCtrl.allUsers);

        _.each(userCtrl.allUsers, function (user) {
            if (!user.username) {
                user.username = user.name;
            }
            if (user.fbUserId) {
                if (user.fbUserId.length == 17) {
                    user.logintype = 'via Facebook';
                    user.image = "http://graph.facebook.com/" + user.fbUserId + "/picture?type=normal"
                }
                else {
                    user.logintype = 'via Gmail';
                }
            }
            else {
                user.logintype = 'via E-mail';

            }
        })
    });


}

function categoryCtrl(Category, Upload, catApi) {
    var categoryCtrl = this;
    categoryCtrl.noImage = false;
    categoryCtrl.ImageStyle = {'width': 80 + 'px', 'height': 80 + 'px'};

    categoryCtrl.category = {}
    categoryCtrl.categoryList = [];
    Category.query({}, function (data) {
            console.log(data)
            categoryCtrl.categoryList = data;
        }

        , function (err) {
            console.log(err)

        })
    console.log(categoryCtrl.categoryNameList);

    categoryCtrl.submitted = false;

    categoryCtrl.addCategory = function () {
        if (categoryCtrl.file != null && categoryCtrl.file != '' && typeof categoryCtrl.file != 'undefined') {
            categoryCtrl.noImage = false;
            categoryCtrl.addNewCategory();
        }
        else {
            categoryCtrl.noImage = true;
        }
    }
    categoryCtrl.addNewCategory = function () {


        var imageName;


        Upload.upload({
            url: catApi + '/uploadImage',
            data: {file: categoryCtrl.file}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            console.log(resp.data);
            categoryCtrl.submitted = false;
            imageName = resp.data.name;
        }, function (resp) {
            console.log('Error status: ' + resp.status);

        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        }).then(function () {
            categoryCtrl.category.image = imageName;
            categoryCtrl.category.imageThumb = imageName;
            Category.save(categoryCtrl.category, function (data) {
                categoryCtrl.submitted = false;
                categoryCtrl.categoryList.push(data);
                console.log(data);
                categoryCtrl.file = null;
                categoryCtrl.category = {};
            }, function (err) {

                console.log(err);
            })
        })
    };


    categoryCtrl.upload = function (file) {
    };


    categoryCtrl.show = false;

    categoryCtrl.update = {};
    categoryCtrl.itemToBeEdited = {};
    categoryCtrl.selectCategory = function (item) {
        categoryCtrl.show = true;
        console.log(item);
        categoryCtrl.itemToBeEdited = item;
        categoryCtrl.itemToBeEditedIndex = item.order;
        categoryCtrl.update._id = item._id;
        categoryCtrl.update.name = item.name;
        categoryCtrl.update.image = item.image;
        categoryCtrl.update.imageThumb = item.imageThumb;
    }

    var imageName;
    categoryCtrl.UpdateCategory = function () {
        var imageName;

        if (categoryCtrl.files) {

            Upload.upload({
                url: catApi + '/uploadImage',
                data: {file: categoryCtrl.files}
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                console.log(resp.data);
                imageName = resp.data.name;
            }, function (resp) {
                console.log('Error status: ' + resp.status);

            }).then(function () {
                categoryCtrl.update.image = imageName;
                categoryCtrl.update.imageThumb = imageName;
                console.log(categoryCtrl.update);
                categoryCtrl.itemToBeEdited = {};
                saveDetails('image');
            })
        }
        else {
            saveDetails('noimage');
        }

        var reorderedData = [];
        _.each(categoryCtrl.categoryList, function (n) {
            reorderedData.push({"_id": n._id, "order": n.order})
        });
        console.log(reorderedData);

        Category.reorder({'reorderedData': reorderedData}, function (data) {
            console.log(data);
            categoryCtrl.itemToBeEdited = null;
            categoryCtrl.show = false;
        });
    }

    function saveDetails(param) {
        categoryCtrl.update.order = categoryCtrl.itemToBeEditedIndex;
        Category.update(categoryCtrl.update, function (data) {
            _.each(categoryCtrl.categoryList, function (category) {
                if (category._id == categoryCtrl.update._id) {
                    category.name = categoryCtrl.update.name;
                    if (param == 'image') {
                        category.image = categoryCtrl.update.image;
                        category.imageThumb = categoryCtrl.update.imageThumb;
                    }
                }
            })

        }, function (err) {
            console.log(err);
        })
    }


    categoryCtrl.DeleteCategory = function () {
        console.log(categoryCtrl.update);
        categoryCtrl.itemToBeEdited = {};
        categoryCtrl.update.order = categoryCtrl.itemToBeEditedIndex;
        categoryCtrl.update.deleted = true;
        Category.update(categoryCtrl.update, function (data) {
            console.log(data);
            categoryCtrl.categoryList = _.reject(categoryCtrl.categoryList, function (category) {
                return category._id == categoryCtrl.update._id;
            })
            categoryCtrl.update.deleted = false;
            categoryCtrl.show = false;

        }, function (err) {
            console.log(err);
            categoryCtrl.update.deleted = false;
        })
    }


    categoryCtrl.orderDown = function () {
        if (categoryCtrl.itemToBeEditedIndex < categoryCtrl.categoryList.length) {
            console.log('entered');
            categoryCtrl.nextItem = _.find(categoryCtrl.categoryList, function (o) {
                return o.order == categoryCtrl.itemToBeEditedIndex + 1;
            });
            categoryCtrl.nextItem.order = categoryCtrl.itemToBeEditedIndex;
            categoryCtrl.itemToBeEdited.order = categoryCtrl.itemToBeEditedIndex + 1;
            categoryCtrl.itemToBeEditedIndex = categoryCtrl.itemToBeEditedIndex + 1;
            console.log(categoryCtrl.categoryList);
        }
    }

    categoryCtrl.orderUp = function () {
        console.log('entered');

        if (categoryCtrl.itemToBeEditedIndex > 1) {
            categoryCtrl.prevItem = _.find(categoryCtrl.categoryList, function (o) {
                return o.order == categoryCtrl.itemToBeEditedIndex - 1;
            });
            categoryCtrl.prevItem.order = categoryCtrl.itemToBeEditedIndex;
            categoryCtrl.itemToBeEdited.order = categoryCtrl.itemToBeEditedIndex - 1;
            categoryCtrl.itemToBeEditedIndex = categoryCtrl.itemToBeEditedIndex - 1;
            console.log(categoryCtrl.categoryList);
        }
    }
}

function addMenuItemCtrl(Category, MenuItem, catApi, Upload, $scope) {
    var addMenuItemCtrl = this;
    addMenuItemCtrl.displayList = [];
    addMenuItemCtrl.menu = {};
    addMenuItemCtrl.noCategory = false;
    addMenuItemCtrl.noImage = false;

    addMenuItemCtrl.dropDetail = function (index) {
        if (addMenuItemCtrl.index == index)
            addMenuItemCtrl.index = -1;
        else
            addMenuItemCtrl.index = index;

    }

    addMenuItemCtrl.categoryList = [];
    addMenuItemCtrl.categoryListfirstName = "";
    $scope.firstName = "";

    //query for all  categoryList in category
    Category.query({}, function (data) {
            console.log(data)
            addMenuItemCtrl.categoryList = data;
            $scope.firstName = addMenuItemCtrl.categoryList[0].name;
            addMenuItemCtrl.menu.selectCategory = addMenuItemCtrl.categoryList[0];

            console.log($scope.firstName);
        }

        , function (err) {
            console.log(err)

        })

    //change the list to be displayed, so that order can be separately shown
    addMenuItemCtrl.changeDisplayList = function () {
        addMenuItemCtrl.displayList = _.filter(addMenuItemCtrl.MenuItemList, function (n) {
            return n.catId == addMenuItemCtrl.menu.selectCategory._id;
        })
    }

    //query for all data item in MenuItem
    addMenuItemCtrl.MenuItemList = [];
    addMenuItemCtrl.MenuItemListfirstName = "";

    MenuItem.query({}, function (data) {
            console.log(data)
            addMenuItemCtrl.MenuItemList = data;
            addMenuItemCtrl.displayList = _.filter(addMenuItemCtrl.MenuItemList, function (n) {
                return n.catId == addMenuItemCtrl.menu.selectCategory._id;
            })
            addMenuItemCtrl.MenuItemListfirstName = addMenuItemCtrl.MenuItemList[0].name;
            console.log(addMenuItemCtrl.MenuItemListfirstName);
        }

        , function (err) {
            console.log(err)

        })

    //get index function for update list
    addMenuItemCtrl.show = false;
    addMenuItemCtrl.update = {};
    addMenuItemCtrl.itemToBeEdited = {};

    addMenuItemCtrl.indexValue = function (list) {
        addMenuItemCtrl.show = true;
        addMenuItemCtrl.itemToBeEdited = list;
        addMenuItemCtrl.itemToBeEditedIndex = list.order;
        addMenuItemCtrl.update.name = list.name;
        addMenuItemCtrl.update._id = list._id;
        addMenuItemCtrl.update.image = list.image;
        addMenuItemCtrl.update.imageThumb = list.imageThumb;
        addMenuItemCtrl.update.description = list.description;
        addMenuItemCtrl.update.catId = list.catId;
    }
    addMenuItemCtrl.show = false;

    console.log(addMenuItemCtrl.categoryList);


    addMenuItemCtrl.addMenuItem = function () {
        if (addMenuItemCtrl.menuItem.selected) {
            addMenuItemCtrl.noCategory = false;
            if (addMenuItemCtrl.file != null && addMenuItemCtrl.file != '' && typeof addMenuItemCtrl.file != 'undefined') {
                addMenuItemCtrl.noImage = false;
                addMenuItemCtrl.addNewMenuItem();
            }
            else {
                addMenuItemCtrl.noImage = true;
            }
        }
        else {
            addMenuItemCtrl.noCategory = true;
        }
    }

    addMenuItemCtrl.addNewMenuItem = function () {
        addMenuItemCtrl.submitted = false;

        var imageName;
        Upload.upload({
            url: catApi + '/uploadImage',
            data: {file: addMenuItemCtrl.file}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            console.log(resp.data);
            addMenuItemCtrl.submitted = false;
            imageName = resp.data.name;
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        }).then(function () {

            console.log(addMenuItemCtrl.menuItem);
            console.log(addMenuItemCtrl.menuItem.selected._id);

            var sendData = {
                name: addMenuItemCtrl.menuItem.name,
                description: addMenuItemCtrl.menuItem.description,
                catId: addMenuItemCtrl.menuItem.selected._id,
                image: imageName,
                imageThumb: imageName

            }

            console.log(sendData);
            MenuItem.save(sendData, function (data) {
                addMenuItemCtrl.submitted = false;
                // addMenuItemCtrl.MenuItemList.push(data);
                console.log(data);
                if (sendData.catId == addMenuItemCtrl.menu.selectCategory._id) {
                    addMenuItemCtrl.displayList.push(data)
                }
                addMenuItemCtrl.file = null;

                addMenuItemCtrl.menuItem = {};
                data = {};
                console.log("menuItem is blank");

            }, function (err) {
                console.log(err);
            });

        })
    }

    addMenuItemCtrl.orderDown = function () {
        console.log('down')
        if (addMenuItemCtrl.itemToBeEditedIndex < addMenuItemCtrl.displayList.length) {

            console.log('entered');
            if (addMenuItemCtrl.index != -1) {
                addMenuItemCtrl.index = addMenuItemCtrl.index + 1;
            }
            addMenuItemCtrl.nextItem = _.find(addMenuItemCtrl.displayList, function (o) {
                return o.order == addMenuItemCtrl.itemToBeEditedIndex + 1;
            });
            addMenuItemCtrl.nextItem.order = addMenuItemCtrl.itemToBeEditedIndex;
            addMenuItemCtrl.itemToBeEdited.order = addMenuItemCtrl.itemToBeEditedIndex + 1;
            addMenuItemCtrl.itemToBeEditedIndex = addMenuItemCtrl.itemToBeEditedIndex + 1;
            console.log(addMenuItemCtrl.displayList);
        }
    }

    addMenuItemCtrl.orderUp = function () {
        console.log('entered');

        if (addMenuItemCtrl.itemToBeEditedIndex > 1) {
            if (addMenuItemCtrl.index != -1) {
                addMenuItemCtrl.index = addMenuItemCtrl.index - 1;
            }
            addMenuItemCtrl.prevItem = _.find(addMenuItemCtrl.displayList, function (o) {
                return o.order == addMenuItemCtrl.itemToBeEditedIndex - 1;
            });
            addMenuItemCtrl.prevItem.order = addMenuItemCtrl.itemToBeEditedIndex;
            addMenuItemCtrl.itemToBeEdited.order = addMenuItemCtrl.itemToBeEditedIndex - 1;
            addMenuItemCtrl.itemToBeEditedIndex = addMenuItemCtrl.itemToBeEditedIndex - 1;
            console.log(addMenuItemCtrl.displayList);
        }
    }

    addMenuItemCtrl.upload = function (file) {
    };

    var imageName;
    addMenuItemCtrl.UpdateCategory = function () {


        if (addMenuItemCtrl.files) {
            Upload.upload({
                url: catApi + '/uploadImage',
                data: {file: addMenuItemCtrl.files}
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                console.log(resp.data);
                imageName = resp.data.name;
                console.log(imageName);
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }).then(function () {

                addMenuItemCtrl.update.image = imageName;
                addMenuItemCtrl.update.imageThumb = imageName;
                console.log(addMenuItemCtrl.update);
                addMenuItemCtrl.itemToBeEdited = {};
                saveDetails('image');
            })
        }
        else {
            saveDetails('noimage');
        }


        var reorderedData = [];
        _.each(addMenuItemCtrl.displayList, function (n) {
            reorderedData.push({"_id": n._id, "order": n.order})
        });
        console.log(reorderedData);

        MenuItem.reorder({'reorderedData': reorderedData}, function (data) {
            console.log(data);
            addMenuItemCtrl.itemToBeEdited = null;
            addMenuItemCtrl.show = false;
        });

    }

    function saveDetails(param) {
        addMenuItemCtrl.update.order = addMenuItemCtrl.itemToBeEditedIndex;

        MenuItem.update(addMenuItemCtrl.update, function (data) {
            console.log(data);
            _.each(addMenuItemCtrl.displayList, function (MenuItem) {
                console.log(MenuItem._id);
                console.log(addMenuItemCtrl.update._id);
                if (MenuItem._id == addMenuItemCtrl.update._id) {
                    MenuItem.name = addMenuItemCtrl.update.name;
                    console.log(imageName);
                    if (param == 'image') {
                        MenuItem.image = imageName;
                        MenuItem.imageThumb = imageName;
                    }
                    MenuItem.description = addMenuItemCtrl.update.description;

                }
            })

        }, function (err) {
            console.log(err);
        })
    }

    addMenuItemCtrl.DeleteCategory = function () {
        console.log(addMenuItemCtrl.update);
        addMenuItemCtrl.itemToBeEdited = {};
        addMenuItemCtrl.update.deleted = true;
        addMenuItemCtrl.update.order = addMenuItemCtrl.itemToBeEditedIndex;
        console.log(addMenuItemCtrl.update.order);
        console.log(addMenuItemCtrl.update);
        console.log(addMenuItemCtrl.displayList);
        MenuItem.update(addMenuItemCtrl.update, function (data) {
            addMenuItemCtrl.show = false;
            addMenuItemCtrl.displayList = _.reject(addMenuItemCtrl.displayList, function (MenuItem) {
                console.log("delete is working")
                console.log(MenuItem._id);
                console.log(addMenuItemCtrl.update._id);
                return MenuItem._id == addMenuItemCtrl.update._id;
            })
            console.log(addMenuItemCtrl.displayList);
            addMenuItemCtrl.update.deleted = false;

        }, function (err) {
            console.log(err);
            addMenuItemCtrl.update.deleted = false;

        })

    }


    addMenuItemCtrl.menuItem = {}

}

function addTourCtrl(TourItem, catApi, Upload) {
    var addTourCtrl = this;

    addTourCtrl.TourItem = {};
    addTourCtrl.noImage = false;

    //query for all data item in TourItem
    addTourCtrl.TourItemList = [];
    TourItem.query({}, function (data) {
            console.log(data)
            addTourCtrl.TourItemList = data;
        }

        , function (err) {
            console.log(err)

        })

    //get index function for update list
    addTourCtrl.update = {};
    addTourCtrl.itemToBeEdited = {};
    addTourCtrl.show = false;


    addTourCtrl.obj = function (selected) {
        addTourCtrl.show = true;
        console.log("its working");
        addTourCtrl.itemToBeEdited = selected;
        addTourCtrl.itemToBeEditedIndex = selected.order;
        console.log(addTourCtrl.itemToBeEdited);

        console.log(addTourCtrl.itemToBeEditedIndex);
        addTourCtrl.update.name = selected.name;
        addTourCtrl.update._id = selected._id;
        addTourCtrl.update.image = selected.image;
        addTourCtrl.update.imageThumb = selected.imageThumb;
        addTourCtrl.update.description = selected.description;

    }
    addTourCtrl.show = false;
    //
    //addTourCtrl.UpdateCategory=function(){
    //    console.log(addTourCtrl.update);
    //    addTourCtrl.itemToBeEdited={};
    //
    //}
    addTourCtrl.addTour = function () {
        if (addTourCtrl.file != null && addTourCtrl.file != '' && typeof addTourCtrl.file != 'undefined') {
            addTourCtrl.noImage = false;
            addTourCtrl.addNewTour();
        }
        else {
            addTourCtrl.noImage = true;
        }
    }

    addTourCtrl.addNewTour = function () {
        console.log("aosj");
        addTourCtrl.submitted = false;

        var imageName;
        Upload.upload({
            url: catApi + '/uploadImage',
            data: {file: addTourCtrl.file}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            console.log(resp.data);
            addTourCtrl.submitted = false;
            imageName = resp.data.name;
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        }).then(function () {
            addTourCtrl.TourItem.image = imageName;
            addTourCtrl.TourItem.imageThumb = imageName;

            console.log(addTourCtrl.TourItem);

            TourItem.save(addTourCtrl.TourItem, function (data) {
                addTourCtrl.submitted = false;
                addTourCtrl.TourItemList.push(data);
                console.log(data);
                addTourCtrl.file = null;

            }, function (err) {
                console.log(err);
            })

        });


    };

    var imageName;
    addTourCtrl.UpdateCategory = function () {

        if (addTourCtrl.files) {
            Upload.upload({
                url: catApi + '/uploadImage',
                data: {file: addTourCtrl.files}
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                console.log(resp.data);
                imageName = resp.data.name;
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            }).then(function () {
                addTourCtrl.update.image = imageName;
                addTourCtrl.update.imageThumb = imageName;
                console.log(addTourCtrl.update);
                addTourCtrl.itemToBeEdited = {};
                saveDetails('image');
            });
        }
        else {
            saveDetails('noimage');
        }

        var reorderedData = [];
        _.each(addTourCtrl.TourItemList, function (n) {
            reorderedData.push({"_id": n._id, "order": n.order})
        });

        TourItem.reorder({'reorderedData': reorderedData}, function (data) {
            console.log(data);
            addTourCtrl.itemToBeEdited = null;
            addTourCtrl.show = false;
        });
    }

    function saveDetails(param) {
        addTourCtrl.update.order = addTourCtrl.itemToBeEditedIndex;

        TourItem.update(addTourCtrl.update, function (data) {
            console.log(data);
            _.each(addTourCtrl.TourItemList, function (TourItem) {
                if (TourItem._id == addTourCtrl.update._id) {
                    TourItem.name = addTourCtrl.update.name;
                    TourItem.description = addTourCtrl.update.description;

                    if (param == 'image') {
                        TourItem.image = imageName;
                        TourItem.imageThumb = imageName;
                    }
                }
            })

        }, function (err) {
            console.log(err);
        })
    }

    addTourCtrl.orderDown = function () {
        console.log('down')
        if (addTourCtrl.itemToBeEditedIndex < addTourCtrl.TourItemList.length) {

            console.log('entered');
            addTourCtrl.nextItem = _.find(addTourCtrl.TourItemList, function (o) {
                return o.order == addTourCtrl.itemToBeEditedIndex + 1;
            });
            console.log(addTourCtrl.nextItem);
            addTourCtrl.nextItem.order = addTourCtrl.itemToBeEditedIndex;
            addTourCtrl.itemToBeEdited.order = addTourCtrl.itemToBeEditedIndex + 1;
            addTourCtrl.itemToBeEditedIndex = addTourCtrl.itemToBeEditedIndex + 1;
            console.log(addTourCtrl.TourItemList);
        }
    }

    addTourCtrl.orderUp = function () {
        console.log('entered');

        if (addTourCtrl.itemToBeEditedIndex > 1) {
            if (addTourCtrl.index != -1) {
                addTourCtrl.index = addTourCtrl.index - 1;
            }
            addTourCtrl.prevItem = _.find(addTourCtrl.TourItemList, function (o) {
                return o.order == addTourCtrl.itemToBeEditedIndex - 1;
            });
            addTourCtrl.prevItem.order = addTourCtrl.itemToBeEditedIndex;
            addTourCtrl.itemToBeEdited.order = addTourCtrl.itemToBeEditedIndex - 1;
            addTourCtrl.itemToBeEditedIndex = addTourCtrl.itemToBeEditedIndex - 1;
            console.log(addTourCtrl.TourItemList);
        }
    }

    addTourCtrl.DeleteCategory = function () {
        console.log(addTourCtrl.update);
        addTourCtrl.itemToBeEdited = {};
        addTourCtrl.update.deleted = true;
        addTourCtrl.update.order = addTourCtrl.itemToBeEditedIndex;

        TourItem.update(addTourCtrl.update, function (data) {
            addTourCtrl.show = false;
            addTourCtrl.TourItemList = _.reject(addTourCtrl.TourItemList, function (TourItem) {
                return TourItem._id == addTourCtrl.update._id;
            })
            addTourCtrl.update.deleted = false;

        }, function (err) {
            console.log(err);
            addTourCtrl.update.deleted = false;

        })
    }
}

function addTourDetailsCtrl(TourItem, TourDetails, catApi, Upload) {
    var addTourDetailsCtrl = this;

    addTourDetailsCtrl.TourDetails = {};
    addTourDetailsCtrl.TourDetailList = [];
    addTourDetailsCtrl.menuItemList = [];
    addTourDetailsCtrl.noTour = false;
    addTourDetailsCtrl.noImage = false;

    //query for all data item in TourItem
    TourItem.query({}, function (data) {
            console.log(data)
            addTourDetailsCtrl.menuItemList = data;
            console.log(data.name);
            addTourDetailsCtrl.menuItemList.select = addTourDetailsCtrl.menuItemList[0];
        }

        , function (err) {
            console.log(err)

        })

    //change the list to be displayed, so that order can be separately shown
    addTourDetailsCtrl.changeDisplayList = function () {
        addTourDetailsCtrl.displayList = _.filter(addTourDetailsCtrl.TourDetailList, function (n) {
            return n.tourId == addTourDetailsCtrl.menuItemList.select._id;
        })
    }


    //query for all data item in TourDetails
    addTourDetailsCtrl.TourDetailList = [];
    TourDetails.query({}, function (data) {
            console.log(data)
            addTourDetailsCtrl.TourDetailList = data;
            addTourDetailsCtrl.displayList = _.filter(addTourDetailsCtrl.TourDetailList, function (n) {
                return n.tourId == addTourDetailsCtrl.menuItemList.select._id;
            })

        }

        , function (err) {
            console.log(err)

        })

    //get index function for update list
    addTourDetailsCtrl.show = false;
    addTourDetailsCtrl.update = {};
    addTourDetailsCtrl.itemToBeEdited = {};

    addTourDetailsCtrl.indexValue = function (list) {
        addTourDetailsCtrl.show = true;
        addTourDetailsCtrl.itemToBeEdited = list;
        addTourDetailsCtrl.itemToBeEditedIndex = list.order;
        console.log(addTourDetailsCtrl.itemToBeEditedIndex);
        addTourDetailsCtrl.update.name = list.name;
        addTourDetailsCtrl.update._id = list._id;
        addTourDetailsCtrl.update.image = list.image;
        addTourDetailsCtrl.update.imageThumb = list.imageThumb;
        addTourDetailsCtrl.update.description = list.description;
        addTourDetailsCtrl.update.price = list.price;
        addTourDetailsCtrl.update.tourId = list.tourId;

    }
    addTourDetailsCtrl.show = false;


    //addTourDetailsCtrl.UpdateCategory=function(){
    //    console.log(addTourCtrl.update);
    //    addTourDetailsCtrl.itemToBeEdited={};
    //
    //}
    addTourDetailsCtrl.addTourDetails = function () {
        if (addTourDetailsCtrl.TourDetails.selected) {
            addTourDetailsCtrl.noTour = false;
            if (addTourDetailsCtrl.file != null && addTourDetailsCtrl.file != '' && typeof addTourDetailsCtrl.file != 'undefined') {
                addTourDetailsCtrl.noImage = false;
                addTourDetailsCtrl.addNewTourDetails();
            }
            else {
                addTourDetailsCtrl.noImage = true;
            }
        }
        else {
            addTourDetailsCtrl.noTour = true;
        }
    }

    var imageName;
    addTourDetailsCtrl.addNewTourDetails = function () {
        addTourDetailsCtrl.submitted = false;


        Upload.upload({
            url: catApi + '/uploadImage',
            data: {file: addTourDetailsCtrl.file}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            console.log(resp.data);
            addTourDetailsCtrl.submitted = false;
            imageName = resp.data.name;
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
        }).then(function () {


            console.log(addTourDetailsCtrl.TourDetails);
            console.log(addTourDetailsCtrl.TourDetails.selected._id);
            var sendData = {
                name: addTourDetailsCtrl.TourDetails.name,
                description: addTourDetailsCtrl.TourDetails.description,
                tourId: addTourDetailsCtrl.TourDetails.selected._id,
                price: addTourDetailsCtrl.TourDetails.price,
                image: imageName,
                imageThumb: imageName

            }

            console.log(sendData);
            TourDetails.save(sendData, function (data) {
                addTourDetailsCtrl.submitted = false;
                addTourDetailsCtrl.displayList.push(data);
                console.log(data);
                sendData = {};
                addTourDetailsCtrl.file = null;
                addTourDetailsCtrl.TourDetails = {};

            }, function (err) {
                console.log(err);
            })
        });

    }


    addTourDetailsCtrl.orderDown = function () {
        console.log('down')
        if (addTourDetailsCtrl.itemToBeEditedIndex < addTourDetailsCtrl.displayList.length) {

            console.log('entered');
            if (addTourDetailsCtrl.index != -1) {
                addTourDetailsCtrl.index = addTourDetailsCtrl.index + 1;
            }
            addTourDetailsCtrl.nextItem = _.find(addTourDetailsCtrl.displayList, function (o) {
                return o.order == addTourDetailsCtrl.itemToBeEditedIndex + 1;
            });
            addTourDetailsCtrl.nextItem.order = addTourDetailsCtrl.itemToBeEditedIndex;
            addTourDetailsCtrl.itemToBeEdited.order = addTourDetailsCtrl.itemToBeEditedIndex + 1;
            addTourDetailsCtrl.itemToBeEditedIndex = addTourDetailsCtrl.itemToBeEditedIndex + 1;
            console.log(addTourDetailsCtrl.displayList);
        }
    }

    addTourDetailsCtrl.orderUp = function () {
        console.log('entered');

        if (addTourDetailsCtrl.itemToBeEditedIndex > 1) {
            if (addTourDetailsCtrl.index != -1) {
                addTourDetailsCtrl.index = addTourDetailsCtrl.index - 1;
            }
            addTourDetailsCtrl.prevItem = _.find(addTourDetailsCtrl.displayList, function (o) {
                return o.order == addTourDetailsCtrl.itemToBeEditedIndex - 1;
            });
            addTourDetailsCtrl.prevItem.order = addTourDetailsCtrl.itemToBeEditedIndex;
            addTourDetailsCtrl.itemToBeEdited.order = addTourDetailsCtrl.itemToBeEditedIndex - 1;
            addTourDetailsCtrl.itemToBeEditedIndex = addTourDetailsCtrl.itemToBeEditedIndex - 1;
            console.log(addTourDetailsCtrl.displayList);
        }
    }

    // var imageName;
    addTourDetailsCtrl.UpdateCategory = function () {

        if (addTourDetailsCtrl.files) {

            Upload.upload({
                url: catApi + '/uploadImage',
                data: {file: addTourDetailsCtrl.files}
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                console.log(resp.data);
                imageName = resp.data.name;
            }, function (resp) {
                console.log('Error status: ' + resp.status);
            }, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            }).then(function () {

                addTourDetailsCtrl.update.image = imageName;
                addTourDetailsCtrl.update.imageThumb = imageName;

                console.log(addTourDetailsCtrl.update);
                addTourDetailsCtrl.itemToBeEdited = {};
                saveDetails('image');

            });

        }
        else {
            saveDetails('noimage');
        }

        var reorderedData = [];
        _.each(addTourDetailsCtrl.displayList, function (n) {
            reorderedData.push({"_id": n._id, "order": n.order})
        });

        TourDetails.reorder({'reorderedData': reorderedData}, function (data) {
            console.log(data);
            addTourDetailsCtrl.itemToBeEdited = null;
            addTourDetailsCtrl.show = false;
        });
    }

    function saveDetails(param) {
        addTourDetailsCtrl.update.order = addTourDetailsCtrl.itemToBeEditedIndex;

        TourDetails.update(addTourDetailsCtrl.update, function (data) {
            console.log(data);
            _.each(addTourDetailsCtrl.displayList, function (TourDetails) {
                console.log(TourDetails._id);
                console.log(addTourDetailsCtrl.update._id);
                if (TourDetails._id == addTourDetailsCtrl.update._id) {
                    TourDetails.name = addTourDetailsCtrl.update.name;
                    if (param == "image") {
                        TourDetails.image = imageName;
                        TourDetails.imageThumb = imageName;
                    }
                    TourDetails.description = addTourDetailsCtrl.update.description;
                    TourDetails.price = addTourDetailsCtrl.update.price;

                }
            })

        }, function (err) {
            console.log(err);
        })
    }


    addTourDetailsCtrl.DeleteCategory = function () {
        console.log(addTourDetailsCtrl.update);
        addTourDetailsCtrl.itemToBeEdited = {};
        addTourDetailsCtrl.update.deleted = true;
        addTourDetailsCtrl.update.order = addTourDetailsCtrl.itemToBeEditedIndex;

        TourDetails.update(addTourDetailsCtrl.update, function (data) {
            addTourDetailsCtrl.show = false;
            console.log(data);

            addTourDetailsCtrl.displayList = _.reject(addTourDetailsCtrl.displayList, function (TourDetails) {
                console.log(TourDetails._id);
                console.log(addTourDetailsCtrl.update._id);
                return TourDetails._id == addTourDetailsCtrl.update._id;
            })
            addTourDetailsCtrl.update.deleted = false;

        }, function (err) {
            console.log(err);
            addTourDetailsCtrl.update.deleted = false;

        })

    }
}

function contactInfoCtrl(RestaurantInfo, Passcode) {

    var contactInfoCtrl = this;
    contactInfoCtrl.newdata = {};
    RestaurantInfo.get({}, function (data) {
            console.log(data);
            contactInfoCtrl.newdata.restaurantName = data.name;
            contactInfoCtrl.newdata.lat = data.location.lat;
            contactInfoCtrl.newdata.long = data.location.long;
            contactInfoCtrl.newdata.address = data.address;
            contactInfoCtrl.newdata.saturday = {};
            contactInfoCtrl.newdata.saturday.openingTime = data.timing[6].openingTime;
            contactInfoCtrl.newdata.saturday.closingTime = data.timing[6].closingTime;
            contactInfoCtrl.newdata.sunday = {};
            contactInfoCtrl.newdata.sunday.openingTime = data.timing[0].openingTime;
            contactInfoCtrl.newdata.sunday.closingTime = data.timing[0].closingTime;
            contactInfoCtrl.newdata.monday = {};
            contactInfoCtrl.newdata.monday.openingTime = data.timing[1].openingTime;
            contactInfoCtrl.newdata.monday.closingTime = data.timing[1].closingTime;
            contactInfoCtrl.newdata.tuesday = {};
            contactInfoCtrl.newdata.tuesday.openingTime = data.timing[2].openingTime;
            contactInfoCtrl.newdata.tuesday.closingTime = data.timing[2].closingTime;
            contactInfoCtrl.newdata.wednesday = {};
            contactInfoCtrl.newdata.wednesday.openingTime = data.timing[3].openingTime;
            contactInfoCtrl.newdata.wednesday.closingTime = data.timing[3].closingTime;
            contactInfoCtrl.newdata.thursday = {};
            contactInfoCtrl.newdata.thursday.openingTime = data.timing[4].openingTime;
            contactInfoCtrl.newdata.thursday.closingTime = data.timing[4].closingTime;
            contactInfoCtrl.newdata.friday = {};
            contactInfoCtrl.newdata.friday.openingTime = data.timing[5].openingTime;
            contactInfoCtrl.newdata.friday.closingTime = data.timing[5].closingTime;
        },
        function (err) {
            console.log(err);
        })

    contactInfoCtrl.weekList = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"];
    console.log(contactInfoCtrl.weekList);

    contactInfoCtrl.info = {};

    contactInfoCtrl.addData = function (newdata, clear) {
        console.log(newdata);
        contactInfoCtrl.info.name = newdata.restaurantName;
        contactInfoCtrl.info.location = {};
        contactInfoCtrl.info.location.lat = newdata.lat;
        contactInfoCtrl.info.location.long = newdata.long;
        contactInfoCtrl.info.address = newdata.address;
        contactInfoCtrl.info.timing = [];
        contactInfoCtrl.info.timing.unshift({
            "openingTime": newdata.saturday.openingTime,
            "closingTime": newdata.saturday.closingTime
        });
        contactInfoCtrl.info.timing.unshift({
            "openingTime": newdata.friday.openingTime,
            "closingTime": newdata.friday.closingTime
        });
        contactInfoCtrl.info.timing.unshift({
            "openingTime": newdata.thursday.openingTime,
            "closingTime": newdata.thursday.closingTime
        });
        contactInfoCtrl.info.timing.unshift({
            "openingTime": newdata.wednesday.openingTime,
            "closingTime": newdata.wednesday.closingTime
        });
        contactInfoCtrl.info.timing.unshift({
            "openingTime": newdata.tuesday.openingTime,
            "closingTime": newdata.tuesday.closingTime
        });
        contactInfoCtrl.info.timing.unshift({
            "openingTime": newdata.monday.openingTime,
            "closingTime": newdata.monday.closingTime
        });
        contactInfoCtrl.info.timing.unshift({
            "openingTime": newdata.sunday.openingTime,
            "closingTime": newdata.sunday.closingTime
        });
        console.log(contactInfoCtrl.info.timing);
        RestaurantInfo.update({"info": contactInfoCtrl.info}, function (data) {
                console.log(data);
            },
            function (err) {
                console.log(err);
            })

    }

    console.log(contactInfoCtrl.info);


    contactInfoCtrl.setPasscode = function () {
        Passcode.save({'passcode': contactInfoCtrl.newdata.passcode}, function (data) {
            console.log(data);
        })
    }
}

function FanCommentCtrl(Comment) {
    var FanCommentCtrl = this;
    console.log("yes its working");
    var skip = 0;
    var limit = 10;

    FanCommentCtrl.comments = [];

    FanCommentCtrl.loadMore = function () {
        skip = FanCommentCtrl.comments.length;
        limit = 10;
        FanCommentCtrl.fetchComments();
    }

    FanCommentCtrl.fetchComments = function () {
        Comment.query({skip: skip, limit: limit}, function (data) {
            console.log(data);
            if (data.length == 0) {
                FanCommentCtrl.noMoreComments = true;
                return;
            }
            var User = {};
            _.each(data, function (n) {

                FanCommentCtrl.noMoreComments = false;

                var currentTime = new Date().getTime();
                var difference = (currentTime - new Date(n.createdAt).getTime()) / 1000;
                var timeago = '';
                if (difference < 300) {
                    timeago = 'Just now'
                }
                else if (difference < 3600) {
                    timeago = Math.round(difference / 60, 0) + ' minutes ago';
                }
                else if (difference < 86400) {
                    timeago = Math.round(difference / (60 * 60), 0) + ' hours ago';
                }
                else if (difference < 2592000) {
                    timeago = Math.round(difference / (60 * 60 * 24), 0) + ' days ago';
                }
                else {
                    timeago = Math.round(difference / (60 * 60 * 24 * 30), 0) + ' months ago';
                }
                FanCommentCtrl.comments.push({
                    '_id': n._id, 'comment': n.comment, 'userId': n.userId, 'time': timeago,
                    'created': new Date(n.createdAt), 'picture': n.picture
                });

            })
            skip = skip + FanCommentCtrl.comments.length;
            console.log(FanCommentCtrl.comments)
        })
    }

    FanCommentCtrl.fetchComments();

    FanCommentCtrl.selectedComment = null;
    FanCommentCtrl.selectThisComment = function (comment, index) {
        FanCommentCtrl.selectedComment = comment;
        FanCommentCtrl.selectedCommentIndex = index;
        console.log(index);
        FanCommentCtrl.showDeleteButton = true;

    }

    FanCommentCtrl.deleteComment = function () {
        Comment.update({"comment": FanCommentCtrl.selectedComment}, function (data) {
            console.log(data);
            FanCommentCtrl.comments = _.reject(FanCommentCtrl.comments, function (o) {
                return o._id == FanCommentCtrl.selectedComment._id;
            })
            FanCommentCtrl.showDeleteButton = false;

        }, function (err) {
            console.log(err);
        })
    }
}

function PushNotificationsCtrl(PushNotification) {
    var PushNotificationsCtrl = this;
    console.log("this is PushNotificationsCtrl its working");
    PushNotificationsCtrl.allMessages = [];
    PushNotificationsCtrl.showDeleteButton = false;

    function loadAllPush() {
        PushNotification.query({}, function (data) {
                console.log(data);
                if (data) {
                    _.each(data, function (n) {
                        console.log(n.message);

                        if (n.message.indexOf("{{") != -1) {
                            var link = n.message.substring(n.message.indexOf("{{") + 2, n.message.indexOf("}}"));
                            console.log(link);

                            var linkText = n.message.substring(n.message.indexOf("<<") + 2, n.message.indexOf(">>"));
                            console.log(linkText);
                            n.message = n.message.replace(/<<|>>|{{|}}/g, '');
                            console.log(n.message);
                            n.message = n.message.replace(link, '');
                            console.log(n.message);
                            n.message = n.message.replace(linkText, ' ' + '<a class="bold" href="#" onclick="window.open(\'' + link + '\', \'_system\')">' + linkText + '</a>');
                            console.log(n.message);
                        }
                        PushNotificationsCtrl.allMessages.push(n);

                    });
                    console.log(PushNotificationsCtrl.allMessages);
                }
            },
            function (err) {
                console.log(err);
            })

    }

    loadAllPush();

    PushNotificationsCtrl.sendPush = function () {
        console.log('push called');
        if (!PushNotificationsCtrl.pushLinkText || !PushNotificationsCtrl.pushLink) {
            PushNotificationsCtrl.pushContentToSave = PushNotificationsCtrl.pushContent;
        }
        console.log(PushNotificationsCtrl.pushContentToSave);
        console.log(PushNotificationsCtrl.pushContent);
        PushNotification.save({
            "message": PushNotificationsCtrl.pushContentToSave,
            "messageToSend": PushNotificationsCtrl.pushContent,
            "state": "sidemenu.push"
        }, function (data) {
            console.log(data);
            // PushNotificationsCtrl.allMessages.push({
            //     "message": PushNotificationsCtrl.pushContent, "createdAt": new Date()
            // });
            // PushNotificationsCtrl.allMessages = _.sortBy(PushNotificationsCtrl.allMessages, 'createdAt');
            PushNotificationsCtrl.pushContent = null;
            PushNotificationsCtrl.pushLinkText = null;
            PushNotificationsCtrl.pushLink = null;
            PushNotificationsCtrl.allMessages = [];
            loadAllPush();
        })
    }

    PushNotificationsCtrl.addLink = function () {
        if (PushNotificationsCtrl.pushLink.substring(0, 4) != 'http') {
            PushNotificationsCtrl.useHttp = true;
            return;
        }
        else {
            PushNotificationsCtrl.useHttp = false;
        }


        if (!PushNotificationsCtrl.pushLinkText || !PushNotificationsCtrl.pushLink) {
            PushNotificationsCtrl.showLinkError = true;
        }
        else {
            PushNotificationsCtrl.showLinkError = false;
            if (typeof PushNotificationsCtrl.pushContent == 'undefined') {
                PushNotificationsCtrl.pushContent = '';
            }

            PushNotificationsCtrl.pushContentToSave = PushNotificationsCtrl.pushContent + '<<' +
                PushNotificationsCtrl.pushLinkText + '>>' + '{{' + PushNotificationsCtrl.pushLink + '}}';

            PushNotificationsCtrl.pushContent = PushNotificationsCtrl.pushContent + ' ' +
                PushNotificationsCtrl.pushLinkText;
        }
    }


    PushNotificationsCtrl.selectedPushIndex = null;
    PushNotificationsCtrl.selectedPushId = null;
    PushNotificationsCtrl.selectThisPush = function (index, push) {
        PushNotificationsCtrl.selectedPushIndex = index;
        PushNotificationsCtrl.selectedPushId = push._id;
        console.log(index);
        PushNotificationsCtrl.showDeleteButton = true;
    }


    PushNotificationsCtrl.deletePush = function () {

        PushNotification.update({"id": PushNotificationsCtrl.selectedPushId}, function (data) {
            console.log(data);
            PushNotificationsCtrl.allMessages = _.reject(PushNotificationsCtrl.allMessages, function (o) {
                return o._id == PushNotificationsCtrl.selectedPushId;
            })
            PushNotificationsCtrl.selectedPushIndex = null;
            PushNotificationsCtrl.selectedPushId = null;
            PushNotificationsCtrl.showDeleteButton = false;

        }, function (err) {
            console.log(err);
        })
    }
}

function LoyaltyCtrl(Loyalty, Upload, catApi) {
    var LoyaltyCtrl = this;
    LoyaltyCtrl.edit = false;
    console.log("this is LoyaltyCtrl its working");
    LoyaltyCtrl.allMessages = [];
    LoyaltyCtrl.update = {};
    LoyaltyCtrl.itemToBeEdited = {};
    LoyaltyCtrl.noImage = false;


    LoyaltyCtrl.selectLoyalty = function (item, index) {
        LoyaltyCtrl.selectedLoyaltyIndex = index;
        LoyaltyCtrl.edit = true;
        console.log(item);
        LoyaltyCtrl.itemToBeEdited = item;
        LoyaltyCtrl.update._id = item._id;
        LoyaltyCtrl.update.name = item.name;
        LoyaltyCtrl.update.description = item.description;
        LoyaltyCtrl.update.stamps = item.stamps;
        LoyaltyCtrl.update.image = catApi + '/uploads/' + item.image;
        console.log(LoyaltyCtrl.update.image);
    }

    LoyaltyCtrl.UpdateCategory = function () {
        var imageName;
        if (LoyaltyCtrl.update.stamps > 0) {
            LoyaltyCtrl.update.onlyPositive = false;
        }
        else {
            LoyaltyCtrl.update.onlyPositive = true;
            return;
        }

        if (LoyaltyCtrl.files) {

            Upload.upload({
                url: catApi + '/uploadImage',
                data: {file: LoyaltyCtrl.files}
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                console.log(resp.data);
                imageName = resp.data.name;
            }, function (resp) {
                console.log('Error status: ' + resp.status);

            }).then(function () {
                LoyaltyCtrl.update.image = imageName;
                console.log(LoyaltyCtrl.update);
                LoyaltyCtrl.itemToBeEdited = {};
                saveDetails('image');
            })
        }
        else {
            console.log(LoyaltyCtrl.itemToBeEdited.image);
            LoyaltyCtrl.update.image = LoyaltyCtrl.itemToBeEdited.image;
            saveDetails('noimage');
        }


    }

    function saveDetails(param) {
        console.log(LoyaltyCtrl.update);
        Loyalty.update(LoyaltyCtrl.update, function (data) {
            loadAllLoyalties();
            LoyaltyCtrl.edit = false;
            LoyaltyCtrl.files = null;
        }, function (err) {
            console.log(err);
            alert(err);
        })
    }

    LoyaltyCtrl.DeleteCategory = function () {
        console.log(LoyaltyCtrl.update);
        LoyaltyCtrl.itemToBeEdited = {};
        LoyaltyCtrl.update.deleted = true;
        Loyalty.update(LoyaltyCtrl.update, function (data) {
            LoyaltyCtrl.allMessages = _.reject(LoyaltyCtrl.allMessages, function (loyalty) {
                return loyalty._id == LoyaltyCtrl.update._id;
            })
            LoyaltyCtrl.edit = false;
            LoyaltyCtrl.update.deleted = false;

        }, function (err) {
            console.log(err);
            alert(err);
            LoyaltyCtrl.update.deleted = false;
        })

    }


    function loadAllLoyalties() {
        LoyaltyCtrl.allMessages = [];
        Loyalty.query({}, function (data) {
                console.log(data);
                _.each(data, function (n) {
                    LoyaltyCtrl.allMessages.push(n);
                })
                console.log(LoyaltyCtrl.allMessages);
            },
            function (err) {
                console.log(err);
            });
        console.log(LoyaltyCtrl.allMessages);

    }

    loadAllLoyalties();

    LoyaltyCtrl.addOffer = function () {
        if (LoyaltyCtrl.file != null && LoyaltyCtrl.file != '' && typeof LoyaltyCtrl.file != 'undefined') {
            LoyaltyCtrl.noImage = false;
            if (LoyaltyCtrl.offerStamps > 0) {
                LoyaltyCtrl.onlyPositive = false;
                LoyaltyCtrl.addNewOffer();
            }
            else {
                LoyaltyCtrl.onlyPositive = true;
            }
        }
        else {
            LoyaltyCtrl.noImage = true;
        }
    }

    var imageName;
    LoyaltyCtrl.addNewOffer = function () {
        if (LoyaltyCtrl.offerName && LoyaltyCtrl.description) {
            console.log('asd');

            Upload.upload({
                url: catApi + '/uploadImage',
                data: {file: LoyaltyCtrl.file}
            }).then(function (resp) {
                console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
                console.log(resp.data);
                imageName = resp.data.name;
            }, function (resp) {
                console.log('Error status: ' + resp.status);

            }).then(function () {

                Loyalty.save({
                    "loyaltyName": LoyaltyCtrl.offerName, "loyaltyDescription": LoyaltyCtrl.description,
                    "image": imageName, "stamps": LoyaltyCtrl.offerStamps
                }, function (data) {
                    console.log(data);
                    LoyaltyCtrl.allMessages.push({
                        "name": LoyaltyCtrl.offerName, "description": LoyaltyCtrl.description,
                        "createdAt": data.createdAt, "stamps": LoyaltyCtrl.offerStamps
                    })
                    LoyaltyCtrl.file = null;
                    LoyaltyCtrl.description = '';
                    LoyaltyCtrl.offerName = '';
                })
            })


        }
    }


}

function ManagePhotosCtrl(Category, catApi, Upload, Image) {
    var ManagePhotosCtrl = this;
    ManagePhotosCtrl.tab = 1;

    ManagePhotosCtrl.userImages = [];
    ManagePhotosCtrl.adminImages = [];
    ManagePhotosCtrl.getAllImages = function () {
        Image.query(function (data) {
                console.log(data);
                ManagePhotosCtrl.userImages = _.filter(data, function (n) {
                    return n.userId != 'admin';
                });
                console.log(ManagePhotosCtrl.userImages);
                ManagePhotosCtrl.adminImages = _.filter(data, function (n) {
                    return n.userId == 'admin';
                })
                console.log(ManagePhotosCtrl.adminImages);

            },
            function (err) {
                console.log(err);
            })
    };

    ManagePhotosCtrl.getAllImages();

    ManagePhotosCtrl.upload = function () {

        if (ManagePhotosCtrl.file != null && ManagePhotosCtrl.file != '' && typeof ManagePhotosCtrl.file != 'undefined') {
            ManagePhotosCtrl.imageRequired = false;
        }
        else {
            ManagePhotosCtrl.imageRequired = true;
            return;
        }


        console.log('upload photo start');
        var imageName;
        Upload.upload({
            url: catApi + '/uploadImage',
            data: {file: ManagePhotosCtrl.file}
        }).then(function (resp) {
            console.log('Success ' + resp.config.data.file.name + 'uploaded. Response: ' + resp.data);
            console.log(resp.data);
            ManagePhotosCtrl.submitted = false;
            Image.save({imageId: resp.data.name, userId: 'admin'}, function (data) {
                    console.log('image saved', data);
                    ManagePhotosCtrl.adminImages.push(data);
                },
                function (err) {
                    console.log(err);
                })
        }, function (resp) {
            console.log('Error status: ' + resp.status);
        }).then(function () {
            ManagePhotosCtrl.getAllImages();
            ManagePhotosCtrl.file = null;
        })
    }

    ManagePhotosCtrl.selectedImage = null;
    ManagePhotosCtrl.selectedImageUser = null;

    ManagePhotosCtrl.selectAdminImage = function (image, index) {
        ManagePhotosCtrl.selectedAdminImageIndex = index;
        ManagePhotosCtrl.selectedImage = image;
        console.log('select image to see', image);

    }
    ManagePhotosCtrl.selectUserImage = function (image, index) {
        ManagePhotosCtrl.selectedUserImageIndex = index;
        ManagePhotosCtrl.selectedImageUser = image;
        console.log('select image to see', image);

    }

    ManagePhotosCtrl.deleteImage = function (type) {

        if (type == 'admin') {
            Image.update({imageId: ManagePhotosCtrl.selectedImage.imageId}, function (data) {
                console.log('imageDeleted');
                ManagePhotosCtrl.adminImages = _.reject(ManagePhotosCtrl.adminImages, function (o) {
                    return o.imageId == ManagePhotosCtrl.selectedImage.imageId;
                })
                ManagePhotosCtrl.selectedImage = null;
            })
        }

        else {
            Image.update({imageId: ManagePhotosCtrl.selectedImageUser.imageId}, function (data) {
                console.log('imageDeleted');
                ManagePhotosCtrl.userImages = _.reject(ManagePhotosCtrl.userImages, function (o) {
                    return o.imageId == ManagePhotosCtrl.selectedImageUser.imageId;
                })
                ManagePhotosCtrl.selectedImageUser = null;

            })
        }

    }

    ManagePhotosCtrl.showTab = function (index) {
        ManagePhotosCtrl.tab = index;
    }
}

function HeaderCtrl($rootScope, logout, $location) {
    var headerCtrl = this;
    var show = 0;

    $rootScope.showDiv = 0;

    headerCtrl.logout = function () {
        var logoutPromise = logout.get();
        logoutPromise['$promise'].then(function (data) {

            $rootScope.loggedIn=false;

            $location.path('/login');

        }, function (err) {
            console.log(err);

        })

    }

    headerCtrl.action = function (show) {

        switch (show) {
            case 0:
                $rootScope.showDiv = 0;
                break;

            case 1:
                $rootScope.showDiv = 1;
                break;

            case 2:
                $rootScope.showDiv = 2;
                break;

            case 3:
                $rootScope.showDiv = 3;
                break;

            case 4:
                $rootScope.showDiv = 4;
                break;
            case 5:
                $rootScope.showDiv = 5;
                break;
            case 6:
                $rootScope.showDiv = 6;
                break;
            case 7:
                $rootScope.showDiv = 7;
                break;
            case 8:
                $rootScope.showDiv = 8;
                break;
            case 9:
                $rootScope.showDiv = 9;
                break;
            case 10:
                $rootScope.showDiv = 10;
                break;
        }
        console.log($rootScope.showDiv);
    }


}

function PasscodeCtrl(Passcode, $timeout) {
    var PasscodeCtrl = this;
    var getAllPasscodesPromise = Passcode.query();

    PasscodeCtrl.allPasscodes = [];
    getAllPasscodesPromise['$promise'].then(function (passcodes) {

        _.each(passcodes, function (pass) {
            var totalUsed=0;  
            totalUsed=pass.monday+pass.tuesday+pass.wednesday+pass.thursday+pass.friday+pass.saturday+pass.sunday;
            pass.checkboxValue = false;
            pass.totalUsed=totalUsed;
            PasscodeCtrl.allPasscodes.push(pass);
        })
    });
    PasscodeCtrl.showAddPasscodeForm = false;
    PasscodeCtrl.showEditPasscodeForm = false;
    PasscodeCtrl.newPasscode = {};

    PasscodeCtrl.addPasscode = function () {
        if (typeof PasscodeCtrl.newPasscode.name != 'undefined' && typeof PasscodeCtrl.newPasscode.passcode != 'undefined' && PasscodeCtrl.newPasscode.name.length != 0 && PasscodeCtrl.newPasscode.passcode.length != 0) {
            var addNewPasscodePromise = Passcode.save(PasscodeCtrl.newPasscode);
            addNewPasscodePromise['$promise'].then(function (data) {
                console.log(data);
                if (data.result) {
                    data.checkboxValue = false;
                    PasscodeCtrl.allPasscodes.push(data.result);
                    BootstrapDialog.alert('Changes saved', function (result) {

                    });
                    PasscodeCtrl.showAddPasscodeForm = false;
                    PasscodeCtrl.newPasscode = {};
                }
                else {
                    if (data.error.code == 11000) {
                        BootstrapDialog.alert('Duplicate Entry', function (result) {

                        });
                    }
                }
            }, function (err) {
                console.log(err);
            })
        }
        else {
            BootstrapDialog.alert('Passcode value and passcode name are required', function (result) {
            });
        }
    }

    PasscodeCtrl.openEditPasscodeForm = function () {
        $timeout(function () {
            PasscodeCtrl.showAddPasscodeForm = false;
            PasscodeCtrl.showEditPasscodeForm = true;
        }, 10);
    }

    PasscodeCtrl.editPassCode = function () {
        if (typeof PasscodeCtrl.editPasscode.name != 'undefined' && typeof PasscodeCtrl.editPasscode.passcode != 'undefined' && PasscodeCtrl.editPasscode.name.length != 0 && PasscodeCtrl.editPasscode.passcode.length != 0) {
            var updatePasscodePromise = Passcode.update({passcodeInfo: PasscodeCtrl.editPasscode});
            updatePasscodePromise['$promise'].then(function (data) {
                PasscodeCtrl.allPasscodes[selectedPasscodeIndex] = PasscodeCtrl.editPasscode;
                PasscodeCtrl.allPasscodes[selectedPasscodeIndex].checkboxValue = false;
                PasscodeCtrl.showEditPasscodeForm = false;

                BootstrapDialog.alert('Changes saved', function (result) {
                    PasscodeCtrl.editPasscode = {};
                });
            }, function (err) {
                console.log(err);
            })
        }
        else {
            BootstrapDialog.alert('Passcode value and passcode name are required', function (result) {
            });
        }
    }
    PasscodeCtrl.confirmDelete = function () {
        BootstrapDialog.confirm('Are you sure you want to delete this passcode?', function (result) {
            if (result) {
                PasscodeCtrl.editPasscode = {};
                PasscodeCtrl.showEditPasscodeForm = false;
                PasscodeCtrl.delete();
            }
        });
    }

    PasscodeCtrl.delete = function () {
        var deletePromise = Passcode.delete({'itemToBeDeletedId': selectedPasscode._id});
        deletePromise['$promise'].then(function (data) {
            PasscodeCtrl.allPasscodes.splice(selectedPasscodeIndex, 1);
            console.log(data);
        }, function (err) {
            console.log(err);
        })
    }


    PasscodeCtrl.confirmReset = function () {
        BootstrapDialog.confirm('Are you sure you want to reset this passcode?', function (result) {
            if (result) {
                PasscodeCtrl.reset();
            }
        });
    }

    PasscodeCtrl.reset = function () {
        var resetPromise = Passcode.resetToZero({'itemToBeReset': selectedPasscode._id});
        resetPromise['$promise'].then(function (data) {
            PasscodeCtrl.allPasscodes[selectedPasscodeIndex].checkboxValue = false;
            PasscodeCtrl.allPasscodes[selectedPasscodeIndex].sunday = 0;
            PasscodeCtrl.allPasscodes[selectedPasscodeIndex].monday = 0;
            PasscodeCtrl.allPasscodes[selectedPasscodeIndex].tuesday = 0;
            PasscodeCtrl.allPasscodes[selectedPasscodeIndex].wednesday = 0;
            PasscodeCtrl.allPasscodes[selectedPasscodeIndex].thursday = 0;
            PasscodeCtrl.allPasscodes[selectedPasscodeIndex].friday = 0;
            PasscodeCtrl.allPasscodes[selectedPasscodeIndex].saturday = 0;

        }, function (err) {
            console.log(err);
        })
    }


    var selectedPasscode, selectedPasscodeIndex;
    PasscodeCtrl.disbaleButtons = true;
    PasscodeCtrl.selectPasscode = function (passcode, index, checkboxValue) {
        _.each(PasscodeCtrl.allPasscodes, function (pass) {
            pass.checkboxValue = false;
        })

        passcode.checkboxValue = checkboxValue;
        if (checkboxValue) {
            selectedPasscode = passcode;
            selectedPasscodeIndex = index;
            PasscodeCtrl.editPasscode = angular.copy(selectedPasscode);
            PasscodeCtrl.disbaleButtons = false;
        }
        else {

            PasscodeCtrl.disbaleButtons = true;
            PasscodeCtrl.showEditPasscodeForm = false;
            PasscodeCtrl.editPasscode = {};
        }
    }


    PasscodeCtrl.showAddOptions = function () {
        PasscodeCtrl.showAddPasscodeForm = true;
    }

    PasscodeCtrl.hideAddOptions = function () {

        PasscodeCtrl.newPasscode = {};
        PasscodeCtrl.editPasscode = {};
        PasscodeCtrl.showAddPasscodeForm = false;
        PasscodeCtrl.showEditPasscodeForm = false;
    }

}

function AdminLoginCtrl(Login, $timeout, $location,$rootScope) {
    $rootScope.loggedIn=false;
    var login = this;
    login.user = {};
    login.login = function () {
        var loginPromise = Login.save(login.user);
        loginPromise['$promise'].then(function (data) {
            $rootScope.loggedIn=true;
            $location.path('/adminPanel');
        }, function (err) {
            console.log(err);
            if (err.status == 401) {
                login.loginError = true;
                $timeout(function () {
                    login.loginError = false;
                }, 3000);
            }
        })
    }
}

