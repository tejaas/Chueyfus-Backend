/**
 * Created by abhishekrathore on 9/23/16.
 */


var Category = require('../models/Category');
var _ = require('underscore');
module.exports = {
    getCategory: getCategory,
    setCategory: setCategory,
    updateCategory: updateCategory,
    updateCategoryOrder: updateCategoryOrder

}

function getCategory(req, res) {

    Category
        .find({deleted: false})
        .exec(function (err, categories) {
            console.log("err", err);
            console.log("cat", categories);
            res.json(categories);
        });
}


function setCategory(req, res) {

    var data = req.body;
    var category = new Category(data);
    var categoryLength = 0;
    Category
        .find({deleted: false})
        .exec(function (err, categories) {
            console.log("err", err);
            console.log("cat", categories);
            categoryLength = categories.length;
            category.order = categoryLength+1;
            
            category.save(function (err, category) {
                if (err) throw err;
                res.send(category);
            })
        });
}

function updateCategory(req, res) {

    var data = req.body;
    var categoryId = data._id;
    var isDelete = data.deleted; 
    delete data['_id'];
    console.dir(data);
    console.log(typeof data.order);

    Category.update({_id: categoryId}, data, {upsert: false}, function (err, cat) {
        if (err) throw err;
        else {
            if(isDelete) {
                Category.update({
                    order: {$gt: data.order},
                    deleted: false
                }, {$inc: {order: -1}}, {multi: true}, function (error, success) {
                    res.send(cat);
                })
            }
            else{
                res.send(cat);
            }
        }
    })
}

function updateCategoryOrder(req, res) {

    var data = req.body.reorderedData;
    var index = 0;
    console.log(data);
    _.each(data, function (item) {
        Category
            .update({_id: item._id}, {$set: {"order": item.order}}, {upsert: false}, function (err, cat) {
                console.log(cat);
                console.dir(item);
                if (err) throw err;

                else index++;

                if(index==data.length){
                    res.json("done");
                }
            })

    });
}