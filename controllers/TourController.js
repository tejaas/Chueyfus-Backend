/**
 * Created by abhishekrathore on 9/23/16.
 */


var Tour=require('../models/Tour');
var _ = require('underscore');

module.exports={
    getTour:getTour,
    setTour:setTour,
    updateTour:updateTour,
    updateTourOrder:updateTourOrder
}
function getTour  (req, res) {

    Tour
        .find({deleted:false})
        .exec(function (err, items) {
            res.json(items);
        });
};


function setTour  (req, res) {

    var data = req.body;
    var item = new Tour(data);
    var itemLength = 0;

    Tour
        .find({deleted:false})
        .exec(function (err, items) {
            itemLength = items.length;
            item.order = itemLength+1;

            item.save(function(err,items){
                if(err) throw err;
                res.send(items);
            })
            // res.json(items);
        });


}


function updateTour (req, res) {

    var data = req.body;
    var tourId = data._id;
    var isDelete = data.deleted;
    console.dir(data);
    delete data['_id'];
    
    
    
    Tour.update({_id:tourId},data,{upsert:false},function(err,tour){
        if(err) throw err;
        else {
            if(isDelete) {
                Tour.update({order: {$gt: data.order}, deleted: false}, {$inc: {order: -1}}, {multi: true}, function (error, success) {
                    if(success)
                    res.send(tour);
                })
            }
            else{
                res.send(tour);
            }
        }
        // res.send(tour);
    })
}

function updateTourOrder(req, res) {

    var data = req.body.reorderedData;
    var index = 0;
    _.each(data, function (item) {
        Tour
            .update({_id: item._id}, {$set: {"order": item.order}}, {upsert: false}, function (err, cat) {
                if (err) throw err;

                else index++;

                if(index==data.length){
                    res.json("done");
                }
            })

    });
}