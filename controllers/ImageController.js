var Image = require('../models/Image');

module.exports = {
    getAllImages: getAllImages,
    addImage: addImage,
    deleteImage:deleteImage

}
function getAllImages(req, res) {

    var skip=parseInt(req.query.skip);
    var limit=parseInt(req.query.limit);
    Image
        .find({"deleted":false})
        .skip(skip)
        .limit(limit)
        .exec(function (err, items) {
            console.log(err);
            res.json(items);
        });
};

function deleteImage(req, res) {
    var imageId=req.body.imageId;
    console.log('iamgeId',imageId);

    Image.update({"imageId":imageId},{$set:{"deleted":true}},{upsert:false},function(err,success){
        console.log(err);
        console.log(success);
        if(success){
            res.json(success);          
        }
        else{
            res.json(err);
        }
    })
}

function addImage(req, res) {
    console.log(req.body);
    var imageDetails = {
        userId: req.body.userId,
        imageId: req.body.imageId
    };

    var imageEntry = new Image(imageDetails)
    imageEntry
        .save(function (err, items) {
            res.json(items);
        })
}