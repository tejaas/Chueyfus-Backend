/**
 * Created by abhishekrathore on 9/23/16.
 */


var Item=require('../models/Item');
var _ = require('underscore');

module.exports={
    getItem:getItem,
    setItem:setItem,
    updateItem:updateItem,
    updateItemOrder: updateItemOrder

}
function getItem  (req, res) {

    Item
        .find({deleted:false})
        .exec(function (err, items) {
            res.json(items);
        });
};


function setItem  (req, res) {

    var data = req.body;
    console.dir(data);
    var item = new Item(data);
    var itemsLength = 0;

    Item
        .find({deleted:false})
        .exec(function (err, items) {
            var categoryItems = _.filter(items,function(n){
                return n.catId == data.catId;
            })
            itemsLength = categoryItems.length;
            item.order = itemsLength+1;
            
            item.save(function(error,items){
                if(error) throw error;
                res.send(items);
            })
            // res.json(items);
        });
}

function updateItem (req, res) {

    var data = req.body;
    console.dir(data);
    var itemId = data._id;
    var isDelete = data.deleted;
    delete data['_id'];
    
    Item.update({_id:itemId},data,{upsert:false},function(err,item){
        if(err) throw err;
        else{
            if(isDelete) {
                Item.update({order: {$gt: data.order}, deleted: false, catId: data.catId}, {$inc: {order: -1}}, {multi: true}, function (error, success) {
                    res.send(item);
                })
            }
            else{
                res.send(item);
            }
        }
        // res.send(item);
    })
}


function updateItemOrder(req, res) {

    var data = req.body.reorderedData;
    var index = 0;
    console.dir(data);
    _.each(data, function (item) {
        Item
            .update({_id: item._id}, {$set: {"order": item.order}}, {upsert: false}, function (err, cat) {
                if (err) throw err;

                else index++;

                if(index==data.length){
                    res.json("done");
                }
            })

    });
}