/**
 * Created by abhishekrathore on 9/23/16.
 */

var Tequila=require('../models/Tequila');
var _ = require('underscore');

module.exports={
    getTequila:getTequila,
    setTequila:setTequila,
    updateTequila:updateTequila,
    updateTequilaOrder:updateTequilaOrder
}
function getTequila  (req, res) {

    Tequila
        .find({deleted:false})
        .exec(function (err, items) {
            res.json(items);
        });
};


function setTequila  (req, res) {

    var data = req.body
    var item = new Tequila(data);
    console.dir(data);
    var itemsLength = 0;

    Tequila
        .find({deleted:false})
        .exec(function (err, items) {
            var tequilas = _.filter(items,function(n){
                return n.tourId == data.tourId;
            })
            itemsLength = tequilas.length;
            item.order = itemsLength+1;

            item.save(function(err,item){
                if(err) throw err;
                res.send(item);
            })
        });
}



function updateTequila(req, res) {

    var data = req.body;
    console.dir(data);
    var itemId = data._id;
    var isDelete = data.deleted;
    delete data['_id'];
    
    
    Tequila.update({_id:itemId},data,{upsert:false},function(err,t){
        if(err) throw err;
        else{
            if(isDelete) {
                Tequila.update({order: {$gt: data.order}, deleted: false, tourId: data.tourId}, {$inc: {order: -1}}, {multi: true}, function (error, success) {
                    res.send(t);
                })
            }
            else{
                res.send(t);
            }
        }
    })
}


function updateTequilaOrder(req, res) {

    var data = req.body.reorderedData;
    var index = 0;
    _.each(data, function (item) {
        Tequila
            .update({_id: item._id}, {$set: {"order": item.order}}, {upsert: false}, function (err, cat) {
                if (err) throw err;

                else index++;

                if(index==data.length){
                    res.json("done");
                }
            })

    });
}


