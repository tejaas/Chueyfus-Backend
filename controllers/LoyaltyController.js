var Loyalty=require('../models/Loyalty');
module.exports={
    createLoyalty:createLoyalty,
    getAllLoyalty:getAllLoyalty,
    updateLoyalty:updateLoyalty
}
function getAllLoyalty  (req, res) {

    Loyalty
        .find({deleted: false})
        .exec(function (err, items) {
            res.json(items);
        });
};


function createLoyalty  (req, res) {

    console.dir(req.body);
    var loyalty = {
        name: req.body.loyaltyName,
        description: req.body.loyaltyDescription,
        image: req.body.image,
        stamps: req.body.stamps
    };

    var loyaltyEntry = new Loyalty(loyalty)
    loyaltyEntry
        .save(function (err, items) {
            res.json(items);
        })
}

function updateLoyalty(req, res) {

    var data = req.body;
    console.log(data);
    var loyaltyId = data._id;
    delete data['_id'];


    Loyalty.update({_id: loyaltyId}, data, {upsert: false}, function (err, cat) {
        if (err) throw err;
        res.send(cat);
    })
}
