var Comment = require('../models/Comment');

module.exports = {
    getAllComments: getAllComments,
    addComment: addComment,
    deleteComment:deleteComment

}
function getAllComments(req, res) {

    var skip=parseInt(req.query.skip);
    var limit=parseInt(req.query.limit);
    Comment
        .find({deleted:false})
        .sort('-createdAt')
        .skip(skip)
        .limit(limit)
        .exec(function (err, items) {
            console.log(err);
            res.json(items);
        });
};

function addComment(req, res) {
    console.log(req.body);
    var commentDetails = {
        userId: req.body.userId,
        comment: req.body.comment,
        picture:req.body.picture
    };

    var commentEntry = new Comment(commentDetails);
    commentEntry
        .save(function (err, items) {
            if(err) throw err;
            res.json(items);
        })
}


function deleteComment(req, res) {
    console.log(req.body);
    var comment = req.body.comment;

    Comment
        .update({"_id":comment._id},{"deleted":true},{upsert:false},function (err, items) {
            if(err) throw err;
            else res.json(items);
        })
}