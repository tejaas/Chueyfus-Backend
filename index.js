/**
 * Module dependencies
 */

var done = false;
var express = require('express'),
    bodyParser = require('body-parser'),
    session = require('express-session'),
    routes = require('./routes'),
    api = require('./routes/api'),
    http = require('http'),
    path = require('path'),
    logger = require('morgan'),
    fs = require('node-fs');

var multer = require('multer');

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/uploads')
    },
    filename: function (req, file, cb) {
        cb(null, file.originalname)
    }
});

var upload = multer({storage: storage})
var mongoose = require('mongoose')
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
mongoose.connect('mongodb://localhost:27017/chueyfus');


//Include Controllers Here
var UserController = require('./controllers/UserController');
var CategoryController = require('./controllers/CategoryController');
var ItemController = require('./controllers/ItemController');
var TourController = require('./controllers/TourController');
var TequilaController = require('./controllers/TequilaController');
var ImageController = require('./controllers/ImageController');
var CommentController = require('./controllers/CommentController');
var RestaurantInfoController = require('./controllers/RestaurantInfoController');
var PushNotificationController = require('./controllers/PushNotificationController');
var PasscodeController = require('./controllers/PasscodeController');
var DeviceTokenController = require('./controllers/DeviceTokenController');
var MenuCommentController = require('./controllers/MenuCommentController');
var LoyaltyController = require('./controllers/LoyaltyController');


// Constants
var DEFAULT_PORT = 80;
var PORT = process.env.PORT || DEFAULT_PORT;

// App
var app = express();
app.set('views', __dirname + '/views');
//app.set('view engine', 'jade');
app.set('view engine', 'ejs');
app.set("jsonp callback", true);
app.set('jsonp callback name', 'code');
app.use(logger('combined'));
app.use(function (req, res, next) {
    //console.log('%s %s %s', req.method, req.url, req.path);
    res.header('Access-Control-Allow-Credentials', true);
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    if ('OPTIONS' == req.method) {
        res.sendStatus(200);
    }
    else {
        next();
    }
});
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(session({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: true
}));
app.use(express.static(path.join(__dirname, 'public')));
//Configure passport middleware
app.use(passport.initialize());
app.use(passport.session());


//user signup strategy

passport.use('local-signup', new LocalStrategy({
        // by default, local strategy uses username and password, we will override with email
        usernameField: 'username',
        passwordField: 'password',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    UserController.userSignUp
));


//user login strategy
passport.use('local-login', new LocalStrategy(
    {
        usernameField: 'username',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    UserController.userLoginMechanism
));

passport.use('local-admin-login', new LocalStrategy(
    {
        usernameField: 'username',
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    UserController.adminLoginMechanism
));


passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (user, done) {
    done(null, user);
});

app.get('/logout', function (req, res) {
    req.logout();
    res.json({"status": "logout"});
});

/**
 * Routes
 */
app.options('*', function (req, res) {
    res.sendStatus(200);
});

// serve index and view partials
app.get('/', routes.index);
app.get('/partials/:name', ensureAuthenticated, routes.partials);
// JSON API
app.get('/api/name', api.name);
app.get('/logout', function (req, res) {
    req.logout();
    res.json({"status": "logout"});
});
app.post('/login', passport.authenticate('local-login'), function (req, res) {
    if (req) {
        res.json({"userData": req.user});
    }
    else {
        console.log("error")
    }
});
app.post('/adminLogin', passport.authenticate('local-admin-login'), function (req, res) {
    if (req) {
        res.json({"userData": req.user});
    }
    else {
        console.log("error")
    }
});


app.post('/user', passport.authenticate('local-signup'), function (req, res) {
    if (req) {
        res.json({"userData": req.user});
    }
    else {
        console.log("error");
    }
});


app.post('/uploadImage', upload.single('file'), function (req, res, next) {
    var d = new Date();
    var path2 = 'public/uploads/' + d.getTime() + path.extname(req.file.originalname);
    console.dir(req.file);
    fs.rename(req.file.path, path2, function (err) {
        if (err) throw err;
        res.send({"name": d.getTime() + path.extname(req.file.originalname)});
    })

});


app.get('/category', CategoryController.getCategory);
app.post('/category', CategoryController.setCategory);
app.put('/category', CategoryController.updateCategory);
app.put('/reorderCategory', CategoryController.updateCategoryOrder);

app.get('/item', ItemController.getItem);
app.post('/item', ItemController.setItem);
app.put('/item', ItemController.updateItem);
app.put('/reorderItems', ItemController.updateItemOrder);

app.get('/tequila', TequilaController.getTequila);
app.post('/tequila', TequilaController.setTequila);
app.put('/tequila', TequilaController.updateTequila);
app.put('/reorderTequila', TequilaController.updateTequilaOrder);

app.get('/tour', TourController.getTour);
app.put('/reorderTour', TourController.updateTourOrder);

app.get('/image', ImageController.getAllImages);
app.post('/image', ImageController.addImage);
app.put('/image', ImageController.deleteImage);
app.get('/comment', CommentController.getAllComments);
app.post('/comment', CommentController.addComment);
app.put('/comment', CommentController.deleteComment);

app.post('/tour', TourController.setTour);
app.put('/tour', TourController.updateTour);
app.post('/fbSignUp', UserController.fbSignUp);
app.post('/checkTequila', UserController.checkTequila);
app.post('/restaurantInfo', RestaurantInfoController.setRestaurantInfo);
app.get('/restaurantInfo', RestaurantInfoController.getRestaurantInfo);
app.put('/restaurantInfo', RestaurantInfoController.editRestaurantInfo);
app.post('/sendPush', PushNotificationController.sendNotificationToCustomer);
app.get('/sendPush', PushNotificationController.getPushNotification);
app.put('/sendPush', PushNotificationController.deletePush);
app.put('/updateDeviceToken', DeviceTokenController.setDeviceToken);
app.post('/passcode', PasscodeController.setPasscode);
app.put('/passcode', PasscodeController.updatePasscodeCount);
app.delete('/passcode/:itemToBeDeletedId', PasscodeController.deletePasscode);
app.put('/passcode/updateInfo', PasscodeController.updatePasscodeInfo);
app.put('/passcode/reset/:itemToBeReset', PasscodeController.resetToZero);
app.get('/passcode', PasscodeController.getPasscode);
app.get('/user', UserController.getAllUsers);
app.get('/menuComment', MenuCommentController.getAllMenuComments);
app.post('/menuComment', MenuCommentController.addMenuComment);
app.post('/forgotPassword', UserController.forgotPassword);
app.post('/changePassword', UserController.changePassword);
app.post('/userLoyalty', UserController.setLoyalty);
app.get('/checkUsername', UserController.checkUsername);
app.post('/loyalty', LoyaltyController.createLoyalty);
app.get('/loyalty', LoyaltyController.getAllLoyalty);
app.put('/loyalty', LoyaltyController.updateLoyalty);


app.get('*', routes.index);
app.listen(PORT);
console.log('Running on http://localhost:' + PORT);

function ensureAuthenticated(req, res, next) {

    console.log('ensureAuthenticated is called');


    if (req.isAuthenticated() && (req.path == "/partials/adminLogin")) {
        console.log('case 1');
        res.status(302);
        res.json({"redirect": "adminPanel"});
    }
    //case if user is not loggedin and is trying to open login or sign up page
    else if (!req.isAuthenticated() && (req.path == "/partials/adminLogin")) {
        console.log('case 2');
        next();
    }
    else if (req.path == "/partials/privacy") {
        console.log('case 5');
        next();
    }
    else if (!req.isAuthenticated() && (req.path == "/partials/adminPanel")) {
        console.log('case 3');
        res.status(302);
        res.json({"redirect": "login"});
    }
    else if (req.isAuthenticated()) {
        next();
    }


}



